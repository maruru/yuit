# yuit

A small, simple, unstyled, data-driven UI

- Can be styled with custom classes
- Can be defined via JS object and queried
- Can be built in immediate mode, all passed object refs are updated with the actual element status (e.g. input value)
- No deps