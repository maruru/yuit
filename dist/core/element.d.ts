import { IElement } from "./element";
import { ISchema } from "./schema";
export declare type TElementConstructor = (schema: ISchema) => IElement;
export interface IElement {
    render(parent: HTMLElement): void;
}
export declare class YUITElement<K extends keyof HTMLElementTagNameMap, S extends ISchema> {
    protected ele: HTMLElementTagNameMap[K];
    protected schema: S;
    constructor(tagName: K, schema: S);
}
