import { ISchema } from "../../src/core/schema";
import { TElementConstructor } from "./element";
declare const loadSchema: (schema: ISchema, parentElement: HTMLElement) => void;
declare const registerElement: (type: string, constructor: TElementConstructor) => void;
export default loadSchema;
export { loadSchema, registerElement };
