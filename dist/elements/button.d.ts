import { IElement, YUITElement } from "../core/element";
import { ISchema } from "../core/schema";
export interface IButton extends IElement {
}
export interface IButtonSchema extends ISchema {
    caption: string;
}
declare class YUITButton extends YUITElement<'button', IButtonSchema> implements IButton {
    constructor(schema: IButtonSchema);
    render(parent: HTMLElement): void;
}
export { YUITButton };
