import { IElement, YUITElement } from "../core/element";
import { ISchema } from "../core/schema";
export interface IContainer extends IElement {
}
export interface IContainerSchema extends ISchema {
    children: ISchema[];
}
declare class YUITContainer extends YUITElement<'div', IContainerSchema> implements IContainer {
    constructor(schema: IContainerSchema);
    render(parent: HTMLElement): void;
}
export { YUITContainer };
