import { IElement } from "../core/element";
import { ISchema } from "../core/schema";
export interface IText extends IElement {
}
export interface ITextSchema extends ISchema {
    name?: undefined;
    text: string;
}
declare class TextElement extends Text implements IText {
    protected schema: ITextSchema;
    constructor(schema: ITextSchema);
    render(parent: HTMLElement): void;
}
export { TextElement };
