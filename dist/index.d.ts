import { ISchema } from "./core/schema";
import './elements/button';
import './elements/container';
import './elements/text';
declare const YUIT: {
    loadSchema(schema: ISchema, parentElement: HTMLElement): void;
};
export default YUIT;
