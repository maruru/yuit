var YUIT = (function () {
    'use strict';

    const elements = new Map();
    const loadSchema = function (schema, parentElement) {
        if (elements.has(schema.type)) {
            elements.get(schema.type)(schema).render(parentElement);
        }
    };
    const registerElement = function (type, constructor) {
        elements.set(type, constructor);
    };

    class YUITElement {
        constructor(tagName, schema) {
            this.ele = document.createElement(tagName);
            this.schema = schema;
            this.ele.setAttribute('data-type', schema.type);
            schema.name && (this.ele.id = schema.name);
            schema.class && this.ele.setAttribute('class', schema.class);
            Object.assign(this.ele, this);
        }
    }

    class YUITButton extends YUITElement {
        constructor(schema) {
            super('button', schema);
            this.ele.textContent = this.schema.caption;
        }
        render(parent) {
            parent.appendChild(this.ele);
        }
    }
    registerElement('button', (schema) => new YUITButton(schema));

    class YUITContainer extends YUITElement {
        constructor(schema) {
            super('div', schema);
        }
        render(parent) {
            let child;
            for (child of this.schema.children) {
                loadSchema(child, this.ele);
            }
            parent.appendChild(this.ele);
        }
    }
    registerElement('container', (schema) => new YUITContainer(schema));

    class TextElement extends Text {
        constructor(schema) {
            super();
            this.schema = schema;
            this.textContent = this.schema.text;
        }
        render(parent) {
            parent.appendChild(this);
        }
    }
    registerElement('text', (schema) => new TextElement(schema));

    const YUIT = {
        loadSchema(schema, parentElement) {
            loadSchema(schema, parentElement);
        }
    };

    return YUIT;

}());
//# sourceMappingURL=yuit.js.map
