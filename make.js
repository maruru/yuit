const fs = require('fs');

const clear = require('rollup-plugin-clear');
const progress = require('rollup-plugin-progress');
const rollup = require('rollup').rollup;
const typescript = require('rollup-plugin-typescript2');
const uglify = require('uglify-js-es6');


(async () => {
    const bundle = await rollup({
        external: [],
        input: 'src/index.ts',
        plugins: [
            clear({
                targets: ['./dist'],
                watch: false,
            }),
            progress({
                clearLine: false,
            }),
            typescript({
                tsconfig: `tsconfig.json`
            }),
        ],
    });

    await bundle.write({
        file: `dist/yuit.js`,
        format: "iife",
        name: 'YUIT',
        sourcemap: true,
    });

    const ugly = uglify.minify('./dist/yuit.js', {
        keep_fnames: true,
        output: {
            comments: /^!/,
        },
    });

    if (ugly.error) {
        console.error(ugly.error);
    }
    else {
        fs.writeFileSync('./dist/yuit.min.js', ugly.code);
    }
})().catch(console.error);
