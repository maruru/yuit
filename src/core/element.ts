import {IElement} from "./element";
import {ISchema} from "./schema";


export type TElementConstructor = (schema: ISchema) => IElement;

export interface IElement {
    render(parent: HTMLElement): void
}

export class YUITElement<K extends keyof HTMLElementTagNameMap, S extends ISchema> {
    protected ele: HTMLElementTagNameMap[K];
    protected schema: S;

    constructor (tagName: K, schema: S) {
        this.ele = document.createElement(tagName);
        this.schema = schema;

        this.ele.setAttribute('data-type', schema.type);
        schema.name && (this.ele.id = schema.name);
        schema.class && this.ele.setAttribute('class', schema.class);
        Object.assign(this.ele, this);
    }
}
