import {ISchema} from "../../src/core/schema";
import {IElement, TElementConstructor} from "./element";


const elements = new Map<string, TElementConstructor>();

const loadSchema = function (schema: ISchema, parentElement: HTMLElement) {
    if (elements.has(schema.type)) {
        // @ts-ignore we check for its existence
        elements.get(schema.type)(schema).render(parentElement);
    }
};

const registerElement = function (type: string, constructor: TElementConstructor) {
    elements.set(type, constructor);
};

export default loadSchema;
export {loadSchema, registerElement};
