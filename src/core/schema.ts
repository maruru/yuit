export interface ISchema {
    class?: string | undefined
    name?: string | undefined
    type: string
}
