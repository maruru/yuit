import {IElement, YUITElement} from "../core/element";
import {ISchema} from "../core/schema";
import {registerElement} from "../core/loader";


export interface IButton extends IElement {}
export interface IButtonSchema extends ISchema {
    caption: string
}

class YUITButton extends YUITElement<'button', IButtonSchema> implements IButton {
    constructor(schema: IButtonSchema) {
        super('button', schema);
        this.ele.textContent = this.schema.caption;
    }

    render(parent: HTMLElement) {
        parent.appendChild(this.ele);
    }
}

registerElement('button', (schema: ISchema) => new YUITButton(schema as IButtonSchema));


export {YUITButton};
