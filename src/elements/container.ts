import {IElement, YUITElement} from "../core/element";
import {ISchema} from "../core/schema";
import {loadSchema, registerElement} from "../core/loader";


export interface IContainer extends IElement {}
export interface IContainerSchema extends ISchema {
    children: ISchema[]
}

class YUITContainer extends YUITElement<'div', IContainerSchema> implements IContainer {
    constructor(schema: IContainerSchema) {
        super('div', schema);
    }

    render(parent: HTMLElement) {
        let child;

        for (child of this.schema.children) {
            loadSchema(child, this.ele);
        }

        parent.appendChild(this.ele);
    }
}

registerElement('container', (schema: ISchema) => new YUITContainer(schema as IContainerSchema));


export {YUITContainer};
