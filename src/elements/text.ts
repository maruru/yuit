import {IElement} from "../core/element";
import {ISchema} from "../core/schema";
import {loadSchema, registerElement} from "../core/loader";


export interface IText extends IElement {}
export interface ITextSchema extends ISchema {
    name?: undefined
    text: string
}

class TextElement extends Text implements IText {
    protected schema: ITextSchema;

    constructor(schema: ITextSchema) {
        super();
        this.schema = schema;
        this.textContent = this.schema.text;
    }

    render(parent: HTMLElement) {
        parent.appendChild(this);
    }
}

registerElement('text', (schema: ISchema) => new TextElement(schema as ITextSchema));


export {TextElement};
