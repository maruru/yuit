import {ISchema} from "./core/schema";
import {loadSchema as load} from "./core/loader";

import './elements/button';
import './elements/container';
import './elements/text';

const YUIT = {
    loadSchema(schema: ISchema, parentElement: HTMLElement) {
        load(schema, parentElement);
    }
};

export default YUIT;
